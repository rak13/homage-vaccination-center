const express = require('express')
const _ = require('lodash')

const router = express.Router()

const { getCenters } = require('../../dao/center')

router.route('/').get(async (req, res) => {
    try {
        return res.status(200).json(await getCenters())
    } catch (err) {
        console.error(err)
        res.status(400).json({ code: err.code, message: 'Error getting centers' })
    }
})
module.exports = router
