const express = require('express')
const _ = require('lodash')

const router = express.Router()

const { getCenter, getCenters } = require('../../dao/center')
const {
    getAllBookings,
    getBookingById,
    createBooking,
    updateBooking,
    deleteBooking
} = require('../../dao/centerBooking')

router.route('/centers').get(async (req, res) => {
    try {
        return res.status(200).json(await getCenters())
    } catch (err) {
        console.error(err)
        res.status(400).json({ code: err.code, message: err.message })
    }
})

router.route('/').get(async (req, res) => {
    try {
        const bookings = await getAllBookings()
        const centers = await getCenters()

        return res.status(200).json(
            Object.values(bookings).map(booking => {
                booking.centerName = centers[booking.centerId].name
                return booking
            })
        )
    } catch (err) {
        console.error(err)
        res.status(400).json({ code: err.code, message: err.message })
    }
})

router.route('/').post(async (req, res) => {
    try {
        const data = req.body
        const bookingId = await createBooking(data)
        res.status(200).json({ bookingId })
    } catch (err) {
        console.error(err)
        res.status(400).json({ code: err.code, message: err.message })
    }
})

router.route('/:bookingId').get(async (req, res) => {
    try {
        const bookingId = req.params.bookingId
        const booking = await getBookingById(bookingId)
        const center = await getCenter(booking.centerId)
        return res.status(200).json({ ...booking, centerName: center.name })
    } catch (err) {
        console.error(err)
        res.status(400).json({ code: err.code, message: err.message })
    }
})

router.route('/:bookingId').post(async (req, res) => {
    try {
        const bookingId = req.params.bookingId
        const data = req.body
        const newBookingId = await updateBooking(bookingId, data)
        return res.status(200).json({ bookingId: newBookingId })
    } catch (err) {
        console.error(err)
        res.status(400).json({ code: err.code, message: err.message })
    }
})

router.route('/:bookingId').delete(async (req, res) => {
    try {
        const bookingId = req.params.bookingId
        await deleteBooking(bookingId)
        return res.status(200).json({ success: true })
    } catch (err) {
        console.error(err)
        res.status(400).json({ code: err.code, message: err.message })
    }
})

module.exports = router
