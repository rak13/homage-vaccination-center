const _ = require('lodash')
const JSONdb = require('simple-json-db')
const { DB_FILE_PATH } = require('../config')

console.log({ DB_FILE_PATH })

const db = new JSONdb(DB_FILE_PATH, { syncOnWrite: true })

/**
 *
 *
 * @param {string} table
 * @param {string} key
 * @param {string} value
 * @returns {void}
 */
async function put(table, key, value) {
    const tableData = db.get(table) || {}
    tableData[key] = value
    db.set(table, tableData)
}

/**
 *
 *
 * @param {string} table
 * @param {string} key
 * @returns {object}
 */
async function get(table, key) {
    const tableData = db.get(table) || {}
    // table === 'centers' && console.log(' --- tableData --- ', _.isEmpty(key), tableData)
    return key ? tableData[key] : tableData
}

/**
 *
 *
 * @param {string} table
 * @param {string} key
 * @returns {object}
 */
async function _delete(table, key) {
    const tableData = db.get(table) || {}
    delete tableData[key]
    db.set(table, tableData)
}

function getAll() {
    return db.JSON()
}

function deleteAll() {
    return db.deleteAll()
}

module.exports = { get, put, delete: _delete, getAll, deleteAll }
