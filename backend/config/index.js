const stages = Object.freeze({
    PROD: 'prod',
    DEV: 'dev',
    TEST: 'test'
})

const config = (() => {
    switch (process.env.STAGE) {
        case stages.PROD:
            console.log(' process.env.STAGE = ', stages.PROD)
            return require(`./${stages.PROD}`)
        case stages.TEST:
            console.log(' process.env.STAGE = ', stages.TEST)
            return require(`./${stages.TEST}`)
        default:
            console.log(' process.env.STAGE = ', stages.DEV)
            return require(`./${stages.DEV}`)
    }
})()

module.exports = config
