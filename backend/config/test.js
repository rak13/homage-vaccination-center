const STAGE = 'test'
const DB_FILE_PATH = `${STAGE}-db.json`

module.exports = {
    STAGE,
    DB_FILE_PATH
}
