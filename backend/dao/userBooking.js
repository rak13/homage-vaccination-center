const _ = require('lodash')
const db = require('../db')

//Data Access Object (DAO) for centers

const tableName = 'user-booking'

async function setBooking(userId, centerId, name, ts) {
    return db.put(tableName, userId, { centerId, name, ts })
}

async function getBooking(userId) {
    return db.get(tableName, userId)
}

async function deleteBooking(userId) {
    return db.delete(tableName, userId)
}

module.exports = {
    tableName,
    getBooking,
    setBooking,
    deleteBooking
}
