const _ = require('lodash')
const db = require('../db')

const { validateCenter } = require('./center')
const { decrementAvailability, incrementAvailability } = require('./centerAvailability')
const {
    getBooking: getUserBooking,
    setBooking: setUserBooking,
    deleteBooking: deleteUserBooking
} = require('./userBooking')

//Data Access Object (DAO) for centers

const tableName = 'center-booking'

function makeKey(centerId, ts) {
    return `${centerId}-${ts}`
}

function splitKey(bookingKey) {
    const [centerId, ts] = bookingKey.split('-')
    return {
        centerId,
        ts: parseInt(ts)
    }
}

async function getAllBookings() {
    return db.get(tableName)
}

async function getBooking(centerId, ts) {
    return db.get(tableName, makeKey(centerId, ts))
}

async function getBookingById(bookingKey) {
    return db.get(tableName, bookingKey)
}

async function _createBooking(centerId, userId, name, ts) {
    await decrementAvailability(centerId, ts)
    await setUserBooking(userId, centerId, name, ts)
    const bookingId = makeKey(centerId, ts)
    await db.put(tableName, bookingId, { centerId, userId, name, ts })
    return bookingId
}

async function _deleteBooking(bookingKey, userId) {
    const { centerId, ts } = splitKey(bookingKey)
    console.log('_deleteBooking', bookingKey, userId, { centerId, ts })

    await incrementAvailability(centerId, ts)
    await deleteBookingSlot(bookingKey)
    await deleteUserBooking(userId)
}

async function deleteBookingSlot(bookingKey) {
    await db.delete(tableName, bookingKey)
}

async function createBooking({ centerId, userId, name, ts }) {
    console.log(' createBooking ', { centerId, userId, name, ts })
    await validateCenter(centerId)
    const userBooking = await getUserBooking(userId)
    if (!_.isEmpty(userBooking)) {
        throw new Error('User has already booked')
    }
    const bookingSlot = await getBooking(centerId, ts)
    if (!_.isEmpty(bookingSlot)) {
        throw new Error('Slot is already taken')
    }
    return await _createBooking(centerId, userId, name, ts)
}

async function updateBooking(prevBookingKey, { centerId, userId, name, ts }) {
    await validateCenter(centerId)

    const prevCenterBooking = await getBookingById(prevBookingKey)
    if (_.isEmpty(prevCenterBooking)) {
        throw new Error('Booking does not exist')
    }
    if (userId !== prevCenterBooking.userId) {
        throw new Error('Must be same user to edit a booking')
    }

    const userBooking = await getUserBooking(userId)
    if (_.isEmpty(userBooking)) {
        throw new Error("User hasn't booked yet")
    }

    console.log({ prevCenterBooking, userBooking, newBooking: { centerId, userId, name, ts } })

    await _deleteBooking(prevBookingKey, userId)
    return await _createBooking(centerId, userId, name, ts)
}

async function deleteBooking(bookingKey) {
    const centerBooking = await getBookingById(bookingKey)
    if (_.isEmpty(centerBooking)) {
        throw new Error('Booking does not exist')
    }
    const { userId } = centerBooking
    const userBooking = await getUserBooking(userId)
    if (_.isEmpty(userBooking)) {
        throw new Error('User booking does not exist')
    }
    _deleteBooking(bookingKey, userId)
}

module.exports = {
    tableName,
    getBooking,
    getAllBookings,
    getBookingById,
    createBooking,
    updateBooking,
    deleteBooking
}
