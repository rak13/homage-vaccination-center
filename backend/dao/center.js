const _ = require('lodash')

const typeDefs = require('../typeDefs/commonTypes')
const db = require('../db')

//Data Access Object (DAO) for centers

const tableName = 'centers'

/**
 * Inserts center information
 *
 * @param {Center} center
 * @returns {void}
 */
async function insertCenter(center) {
    return db.put(tableName, center.centerId, center)
}

async function getCenter(centerId) {
    console.log('getCenter', centerId)
    return db.get(tableName, centerId)
}

async function getCenters() {
    return db.get(tableName)
}

async function validateCenter(centerId) {
    const center = await getCenter(centerId)
    if (_.isEmpty(center)) {
        throw new Error('Center cannot be found')
    }
}

//for test puposes only,
//can be moved to a seed script
async function seedDummyData() {
    const data = [
        { name: 'Bukit Batok CC', id: 1 },
        { name: 'Bukit Panjang CC', id: 2 },
        { name: 'Bukit Timah CC', id: 3 },
        { name: 'Outram Park Polyclinic', id: 4 }
    ]

    for (let center of data) {
        const { id: centerId, name } = center
        await insertCenter({
            centerId,
            name
        })
    }
}

seedDummyData()

module.exports = {
    tableName,
    insertCenter,
    getCenter,
    getCenters,
    validateCenter
}
