const _ = require('lodash')

const typeDefs = require('../typeDefs/commonTypes')
const db = require('../db')
const { getDayTs } = require('./utils')

//Data Access Object (DAO) for centers

/*
 * capacity of a center can vary day to day
 */

const tableName = 'center-availability'

function makeKey(centerId, ts) {
    return `${centerId}-${ts}`
}

/**
 * Inserts center information
 *
 * @param {centerId} centerId
 * @param {number} ts
 * @param {number} capacity
 * @param {number} availability
 * @returns {void}
 */
async function updateCapacity(centerId, ts, capacity, availability) {
    const dayTs = getDayTs(ts)
    const existingCapacity = db.get(tableName, centerId) || {}
    availability = existingCapacity.availability || availability
    return db.put(tableName, makeKey(centerId, dayTs), { capacity, availability })
}

async function getAvailability(centerId, ts) {
    const dayTs = getDayTs(ts)
    return db.get(tableName, makeKey(centerId, dayTs))
}

async function decrementAvailability(centerId, ts) {
    const dayTs = getDayTs(ts)
    const key = makeKey(centerId, dayTs)
    const availabilityObj = await db.get(tableName, key)
    if (_.isEmpty(availabilityObj)) {
        throw new Error('Center is unavailable on selected date or time')
    }
    console.log('decrementAvailability', { key, centerId, ts, dayTs, availabilityObj })

    let { availability } = availabilityObj
    if (availability === 0) {
        throw new Error('Center is fully booked')
    }
    availability -= 1
    return db.put(tableName, makeKey(centerId, dayTs), { ...availabilityObj, availability })
}

async function incrementAvailability(centerId, ts) {
    const dayTs = getDayTs(ts)

    const key = makeKey(centerId, dayTs)
    const availabilityObj = await db.get(tableName, key)
    console.log('incrementAvailability', key, centerId, ts, getDayTs(ts), availabilityObj)

    let { capacity, availability } = availabilityObj
    availability += 1

    if (availability >= capacity) {
        console.warn('Center is fully available ', { centerId, ts, capacity, availability })
        availability = capacity
    }

    return db.put(tableName, makeKey(centerId, dayTs), { ...availabilityObj, availability })
}

//for test puposes only,
//can be moved to a seed script
async function seedDummyData() {
    const data = [
        { name: 'Bukit Batok CC', id: 1 },
        { name: 'Bukit Panjang CC', id: 2 },
        { name: 'Bukit Timah CC', id: 3 },
        { name: 'Outram Park Polyclinic', id: 4 }
    ]

    const today = new Date()
    today.setHours(0, 0, 0, 0)
    let nextDateTs = today.getTime()

    for (let i = 0; i < 5; i++) {
        for (let center of data) {
            const { id: centerId } = center
            const randomCapacity = Math.floor(Math.random() * 20) + 2

            await updateCapacity(centerId, nextDateTs, randomCapacity, randomCapacity)
        }
        nextDateTs = today.setDate(today.getDate() + 1)
    }
}

seedDummyData()

module.exports = {
    tableName,
    updateCapacity,
    getAvailability,
    decrementAvailability,
    incrementAvailability
}
