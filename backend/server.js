const express = require('express')
const cors = require('cors')

const { STAGE } = require('./config')
const db = require('./db')
const bookingsRoute = require('./routes/api/bookings')
const centersRoute = require('./routes/api/centers')

const logger = require('morgan')

const config = require('./config')

const PORT = process.env.PORT || 4000

const app = express()

app.use(
    logger('Method: :method, URL: :url, Status: :status, Response Time: :response-time', {
        skip: (req, res) => STAGE === 'test'
    })
)
app.use(cors({ origin: true }))
app.use(express.json({ limit: '50mb' }))

app.use('/centers', centersRoute)
app.use('/bookings', bookingsRoute)

app.get('/', (req, res) => {
    res.send(`
      <html>
        <title>Rakib Ahsan - Interview</title>
      </html>
    `)
})

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
})

module.exports = app
