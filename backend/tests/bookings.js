process.env.STAGE = 'test'

const chance = new (require('chance'))()
const chai = require('chai')
chai.use(require('chai-http'))

const _ = require('lodash')
const expect = chai.expect

const server = require('../server')

const db = require('../db')
const centerDao = require('../dao/center')
const centerAvailabilityDao = require('../dao/centerAvailability')
const centerBookingDao = require('../dao/centerBooking')
const userBookingDao = require('../dao/centerBooking')

describe('Bookings => ', () => {
    let requester = null

    let testCenter = {
        centerId: 100,
        name: 'TestCenterName'
    }
    let testCenter2 = {
        centerId: 101,
        name: 'TestCenterName2'
    }
    before(async () => {
        requester = chai.request(server).keepOpen()
        await centerDao.insertCenter(testCenter)
        await centerDao.insertCenter(testCenter2)

        const today = new Date()
        today.setHours(0, 0, 0, 0)
        let nextDateTs = today.getTime()

        //5 days
        for (let i = 0; i < 5; i++) {
            await centerAvailabilityDao.updateCapacity(testCenter.centerId, nextDateTs, 10, 10)
            await centerAvailabilityDao.updateCapacity(testCenter2.centerId, nextDateTs, 10, 10)
            nextDateTs = today.setDate(today.getDate() + 1)
        }
    })

    beforeEach(async () => {})

    after(async () => {
        // await db.deleteAll()
    })

    //TODO: validate existence of data in each database

    it('it should get all centers', async () => {
        const response = await requester.get('/centers')
        expect(response).to.have.status(200)
        expect(response.body).to.be.a('object')
        const centers = Object.values(response.body)
        centers.forEach(center => {
            expect(center).to.include.keys('centerId', 'name')
        })
    })

    it('it should book a new slot', async () => {
        let date = new Date()
        date.setDate(date.getDate() + 1)

        const bookingPayload = {
            userId: chance.integer({ min: 1000000 }),
            name: chance.name(),
            centerId: testCenter.centerId,
            ts: date.getTime()
        }
        const response = await requester.post('/bookings').send(bookingPayload)
        expect(response).to.have.status(200)
        expect(response.body).to.be.a('object')
        expect(response.body).to.include.keys('bookingId')
        expect(response.body.bookingId).to.be.equal(bookingPayload.centerId + '-' + bookingPayload.ts)
    })

    it('it should not allow a user to book another slot', async () => {
        let date = new Date()
        date.setDate(date.getDate() + 1)

        const bookingPayload = {
            userId: chance.cf(),
            name: chance.name(),
            centerId: testCenter.centerId,
            ts: date.getTime()
        }
        const response = await requester.post('/bookings').send(bookingPayload)

        const anotherBookingPayload = {
            ...bookingPayload,
            centerId: testCenter2.centerId
        }

        const anotherResponse = await requester.post('/bookings').send(anotherBookingPayload)
        expect(anotherResponse).to.have.status(400)
        expect(anotherResponse.body).to.be.deep.equal({ message: 'User has already booked' })
    })

    it('it should not allow another user to book same slot in same center', async () => {
        let date = new Date()
        date.setDate(date.getDate() + 1)

        const bookingPayload = {
            userId: chance.cf(),
            name: chance.name(),
            centerId: testCenter.centerId,
            ts: date.getTime()
        }
        const response = await requester.post('/bookings').send(bookingPayload)

        const anotherBookingPayload = {
            ...bookingPayload,
            userId: chance.cf()
        }

        const anotherResponse = await requester.post('/bookings').send(anotherBookingPayload)
        expect(anotherResponse).to.have.status(400)
        expect(anotherResponse.body).to.be.deep.equal({ message: 'Slot is already taken' })
    })

    it('it should allow another user to book same slot in different center', async () => {
        let date = new Date()
        date.setDate(date.getDate() + 1)

        const bookingPayload = {
            userId: chance.cf(),
            name: chance.name(),
            centerId: testCenter.centerId,
            ts: date.getTime()
        }
        const response = await requester.post('/bookings').send(bookingPayload)

        const anotherBookingPayload = {
            ...bookingPayload,
            userId: chance.cf(),
            centerId: testCenter2.centerId
        }

        const anotherResponse = await requester.post('/bookings').send(anotherBookingPayload)
        expect(anotherResponse).to.have.status(200)
        expect(anotherResponse.body).to.be.a('object')
        expect(anotherResponse.body).to.include.keys('bookingId')
        expect(anotherResponse.body.bookingId).to.be.equal(anotherBookingPayload.centerId + '-' + bookingPayload.ts)
    })

    //edit booking
    it('it should allow user to edit an exisiting booking', async () => {
        let date = new Date()
        date.setDate(date.getDate() + 1)

        const bookingPayload = {
            userId: chance.cf(),
            name: chance.name(),
            centerId: testCenter.centerId,
            ts: date.getTime()
        }
        const response = await requester.post('/bookings').send(bookingPayload)
        const bookingId = response.body.bookingId

        const editPayload = { ...bookingPayload, centerId: testCenter2.centerId }
        const editResponse = await requester.post(`/bookings/${bookingId}`).send(editPayload)

        expect(editResponse).to.have.status(200)
        expect(editResponse.body).to.be.a('object')
        expect(editResponse.body).to.include.keys('bookingId')
        expect(editResponse.body.bookingId).to.be.equal(editPayload.centerId + '-' + editPayload.ts)

        const previousBooking = await centerBookingDao.getBookingById(bookingId)
        expect(_.isEmpty(previousBooking)).to.be.true
        expect(await centerBookingDao.getBookingById(editResponse.body.bookingId)).to.deep.equal({
            centerId: testCenter2.centerId,
            userId: editPayload.userId,
            name: editPayload.name,
            ts: editPayload.ts
        })
    })

    it('it should delete a booking', async () => {
        let date = new Date()
        date.setDate(date.getDate() + 1)

        const bookingPayload = {
            userId: chance.integer({ min: 1000000 }),
            name: chance.name(),
            centerId: testCenter.centerId,
            ts: date.getTime()
        }
        const response = await requester.post('/bookings').send(bookingPayload)
        const bookingId = response.body.bookingId

        await requester.delete(`/bookings/${bookingId}`)

        const previousBooking = await centerBookingDao.getBookingById(bookingId)
        expect(_.isEmpty(previousBooking)).to.be.true
    })

    //TODO:

    //decrement availability
    //delete booking
    //increment availability
})
