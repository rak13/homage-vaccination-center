import {
  Container,
  Box,
  Button,
  Typography,
  CssBaseline,
  TextField,
  Select,
  MenuItem,
  InputLabel,
} from "@mui/material";
import DateTimePicker from "@mui/lab/DateTimePicker";
import React, { useState, useEffect } from "react";
import { useLocation, useHistory } from "react-router-dom";
import client from "../../utils/client";

export function EditVaccineRegistration() {
  const location = useLocation();
  const history = useHistory();

  const [centers, setCenters] = useState([]);
  const [nric, setNric] = useState("");
  const [name, setName] = useState("");
  const [centerId, setCenterId] = useState(1);
  const [date, setDate] = useState(new Date());
  const [bookingId, setBookingId] = useState();

  useEffect(() => {
    async function getVaccineCenters() {
      try {
        const centersResponse = await client.get("/centers");
        const { data } = centersResponse;
        setCenters(Object.values(data));
        const bookingId = location.pathname.split("/bookings/")[1];
        setBookingId(bookingId);

        const bookingResponse = await client.get(`/bookings/${bookingId}`);
        const { data: booking } = bookingResponse;
        setNric(booking.userId);
        setName(booking.name);
        setCenterId(booking.centerId);
        setDate(booking.ts);
        console.log({ bookingResponse });
      } catch (e) {
        console.error(e);
      }
    }
    getVaccineCenters();
  }, [location]);

  async function handleEdit() {
    try {
      const res = await client.post(`/bookings/${bookingId}`, {
        userId: nric,
        name,
        centerId,
        ts: new Date(date).setSeconds(0),
      });
      const { status, data } = res;
      console.log({ status, data });
      history.push("/bookings");
    } catch (e) {
      console.error(e);
    }
  }

  function handleSelect(event) {
    setCenterId(event.target.value);
  }

  function handleDateChange(value) {
    setDate(value);
  }

  return (
    <React.Fragment>
      <CssBaseline />
      <Container>
        <Box
          component="form"
          sx={{
            mt: 8,
          }}
        >
          <Typography component="h1" variant="h5">
            Book a slot
          </Typography>
          <TextField
            margin="normal"
            required
            fullWidth
            id="nric"
            label="NRIC Number"
            name="NRIC"
            autoComplete="nric"
            sx={{ mb: 2 }}
            autoFocus
            value={nric}
            onChange={(e) => setNric(e.target.value)}
          />
          <TextField
            required
            fullWidth
            id="name"
            label="Full Name"
            name="name"
            autoComplete="name"
            sx={{ mb: 2 }}
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <InputLabel id="vaccineCenterLabel">Vaccine Center</InputLabel>
          <Select
            labelId="vaccineCenterLabel"
            label="Vaccine Center"
            required
            fullWidth
            id="vaccineCenter"
            value={centerId}
            onChange={handleSelect}
            sx={{ mb: 2 }}
          >
            {centers.map((v) => {
              return (
                <MenuItem key={v.centerId} value={v.centerId}>
                  {v.name}
                </MenuItem>
              );
            })}
          </Select>
          <DateTimePicker
            renderInput={(props) => <TextField {...props} />}
            label="Slot"
            value={new Date(date)}
            onChange={handleDateChange}
            minutesStep={15}
            required
          />
          <Button
            // type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
            onClick={handleEdit}
          >
            Register!
          </Button>
        </Box>
      </Container>
    </React.Fragment>
  );
}
