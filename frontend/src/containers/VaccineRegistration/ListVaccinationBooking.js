import {
  Table,
  Box,
  Button,
  CssBaseline,
  Typography,
  TableContainer,
  TableCell,
  TableBody,
  TableRow,
  TableHead,
  Container,
} from "@mui/material";
import { Link } from "react-router-dom";
import ModeEditIcon from "@mui/icons-material/ModeEdit";
import DeleteIcon from "@mui/icons-material/Delete";
import React, { useState, useEffect } from "react";
import client from "../../utils/client";

export function VaccineRegistrationListing() {
  const [bookings, setBookings] = useState([]);

  async function getBookings() {
    try {
      const response = await client.get("/bookings");
      const { status, data } = response;
      console.log("bookings", { status, data });
      setBookings(data);
    } catch (e) {
      console.error(e);
    }
  }

  useEffect(() => {
    getBookings();
  }, []);

  async function handleDelete({ centerId, ts }) {
    try {
      const response = await client.delete(`/bookings/${centerId}-${ts}`);
      const { status, data } = response;
      console.log(" delete booking", { status, data });
      await getBookings();
    } catch (e) {
      console.error(e);
    }
  }
  return (
    <React.Fragment>
      <CssBaseline />
      <Container>
        <Box sx={{ mt: 8 }}>
          <Typography component="h1" variant="h5">
            Active Booking
          </Typography>
          <TableContainer component={Box}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Id</TableCell>
                  <TableCell align="left">Name</TableCell>
                  <TableCell align="left">Center Name</TableCell>
                  <TableCell align="left">Start Time</TableCell>
                  <TableCell align="left">&nbsp;</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {bookings.map((row) => (
                  <TableRow
                    key={`${row.centerId}-${row.ts}`}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.userId}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell align="left">{row.centerName}</TableCell>
                    <TableCell align="left">
                      {new Date(row.ts).toString()}
                    </TableCell>
                    <TableCell align="left">
                      <Button
                        component={Link}
                        to={`/bookings/${row.centerId}-${row.ts}`}
                      >
                        <ModeEditIcon />
                      </Button>
                      <Button onClick={() => handleDelete(row)}>
                        <DeleteIcon />
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      </Container>
    </React.Fragment>
  );
}
