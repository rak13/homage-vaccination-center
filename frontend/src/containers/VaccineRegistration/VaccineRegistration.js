import {
  Container,
  Box,
  Button,
  Typography,
  CssBaseline,
  TextField,
  Select,
  MenuItem,
  InputLabel,
} from "@mui/material";
import DateTimePicker from "@mui/lab/DateTimePicker";
import React, { useState, useEffect } from "react";

import { useHistory } from "react-router-dom";
import client from "../../utils/client";

export function VaccineRegistration() {
  const history = useHistory();

  const [centers, setCenters] = useState([]);
  const [nric, setNric] = useState("");
  const [name, setName] = useState("");
  const [centerId, setCenterId] = useState(1);
  const [date, setDate] = useState(new Date());

  useEffect(() => {
    async function getVaccineCenters() {
      try {
        const response = await client.get("/centers");
        const { status, data } = response;
        console.log({ status, data });
        setCenters(Object.values(data));
      } catch (e) {
        console.error(e);
      }
    }
    getVaccineCenters();
  }, []);

  function handleSelect(event) {
    setCenterId(event.target.value);
  }

  function handleDateChange(value) {
    setDate(value);
  }

  async function handleRegister() {
    try {
      const res = await client.post("/bookings", {
        userId: nric,
        name,
        centerId,
        ts: new Date(date).setSeconds(0),
      });
      const { status, data } = res;
      console.log({ status, data });
      history.push("/bookings");
    } catch (e) {
      console.error(e);
    }
  }

  return (
    <React.Fragment>
      <CssBaseline />
      <Container>
        <Box
          component="form"
          sx={{
            mt: 8,
          }}
        >
          <Typography component="h1" variant="h5">
            Book a slot
          </Typography>
          <TextField
            margin="normal"
            required
            fullWidth
            id="nric"
            label="NRIC Number"
            name="NRIC"
            autoComplete="nric"
            sx={{ mb: 2 }}
            autoFocus
            value={nric}
            onChange={(e) => setNric(e.target.value)}
          />
          <TextField
            required
            fullWidth
            id="name"
            label="Full Name"
            name="name"
            autoComplete="name"
            sx={{ mb: 2 }}
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <InputLabel id="vaccineCenterLabel">Vaccine Center</InputLabel>
          <Select
            labelId="vaccineCenterLabel"
            label="Vaccine Center"
            required
            fullWidth
            id="vaccineCenter"
            value={centerId}
            onChange={handleSelect}
            sx={{ mb: 2 }}
          >
            {centers.map((v) => {
              return (
                <MenuItem key={v.centerId} value={v.centerId}>
                  {v.name}
                </MenuItem>
              );
            })}
          </Select>
          <DateTimePicker
            renderInput={(props) => <TextField {...props} />}
            label="Slot"
            value={date}
            onChange={handleDateChange}
            minutesStep={15}
          />
          <Button
            // type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
            onClick={handleRegister}
          >
            Register!
          </Button>
        </Box>
      </Container>
    </React.Fragment>
  );
}
